# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import journal
from . import move


def register():
    Pool.register(
        journal.Journal,
        move.Move,
        module='account_update_posted', type_='model')
    Pool.register(
        move.MoveBankStatement,
        module='account_update_posted', type_='model',
        depends=['account_bank_statement'])
