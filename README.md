datalife_account_update_posted
==============================

The account_update_posted module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_update_posted/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_update_posted)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
