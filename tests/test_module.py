# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountUpdatePostedTestCase(ModuleTestCase):
    """Test Account Update Posted module"""
    module = 'account_update_posted'


del ModuleTestCase
